import Axios from 'axios';

class ApiHelper {
  static getUserLocation() {
    return Axios({
      method: 'get',
      url: 'https://ipgeolocation.abstractapi.com/v1/?api_key=7cc0d67a90d44d3f881e465c6e742742',
    });
  }
}

export default ApiHelper;
